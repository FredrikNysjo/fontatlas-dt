fontatlas-dt

Description
-----------

Tool for generating font atlases for signed distance field text rendering.

Installation
------------

Requires Python 2.7, numpy, scipy, matplotlib, and freetype-py.

Installation under Ubuntu/Mint:

sudo apt-get install python2.7 python-numpy python-scipy python-matplotlib

freetype-py is already included under externals/.

Usage
-----

For usage instructions, run

./main.py -h

Licensing
---------

See LICENSE.txt.

Third-party libraries
---------------------

freetype-py is distributed under the BSD license.
