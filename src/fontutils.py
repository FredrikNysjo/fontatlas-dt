"""
.. module:: fontutils
   :platform: Unix
   :synopsis: Font utilities.

.. moduleauthor:: Fredrik Nysjo

"""

import sys
import fontatlas
sys.path.insert(0, '../external')
import freetype


def load_face(name, size):
    """Loads a font face specified by name and size in pixels"""
    face = freetype.Face(name)
    face.set_pixel_sizes(size[0], size[1])
    return face
