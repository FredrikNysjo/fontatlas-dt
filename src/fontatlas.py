"""
.. module:: fontatlas
   :platform: Unix
   :synopsis: Font atlas module.

.. moduleauthor:: Fredrik Nysjo

"""

import imageutils

import sys
import os
import json

import numpy
import scipy
import scipy.ndimage
import matplotlib as mpl
import matplotlib.image


class GlyphInfo(object):
    """Stores glyph info.

    Attributes:
        position: Position [x, y] in atlas, in pixels.
        size: Size [w, h] in atlas, in pixels.
        offset: Offset [x, y] from cursor, in pixels.
        advance: Amount [x, y] to move cursor for next char, in pixels.
    """

    def __init__(self):
        self.position = [0.0, 0.0]
        self.size = [0.0, 0.0]
        self.offset = [0.0, 0.0]
        self.advance = [0.0, 0.0]


class Grid(object):
    """Grid function used by FontAtlas.

    Attributes:
        bounds: Grid bounds, e.g., [512, 512].
        cell_size: Grid cell size.
        position: Current grid position.
    """

    def __init__(self, bounds, ft_face, padding=0):
        self.bounds = bounds
        self.cell_size = [ft_face.size.x_ppem + 2 * padding, ft_face.size.y_ppem + 2 * padding]
        self.position = [0, 0]
        self.padding = padding

    def advance(self):
        """Advance grid position."""
        self.position[0] += self.cell_size[0]
        if ((self.position[0] + self.cell_size[0]) / self.bounds[0]) >= 1:
            self.position[0] = 0
            self.position[1] += self.cell_size[1]
        if ((self.position[1] + self.cell_size[1]) / self.bounds[1]) >= 1:
            sys.exit("Error: Position outside grid bounds")


class FontAtlas(object):
    """Stores a font atlas.

    Attributes:
        ft_face: FreeType face to create atlas from.
        ft_face_hires: Hi-res version of face used to compute signed DT.
        glyphs: Glyph info for characters stored in atlas.
        size: Atlas texture size.
        texture: Atlas texture.
        charset: Character set to create atlas from.
        padding: Glyph padding.
    """

    def __init__(self):
        self.ft_face = None
        self.ft_face_hires = None
        self.glyphs = {}
        self.size = [512, 512]
        self.texture = None
        self.charset = range(32, 127)  # Printable ASCII range.
        self.padding = 0

    def update(self):
        """Update font atlas from current font face and character set."""
        # Generate new font atlas.
        texture = numpy.zeros(self.size, numpy.uint8)
        grid = Grid(self.size, self.ft_face, self.padding)
        glyphs = {}
        for c in self.charset:
            self.ft_face.load_char(chr(c))
            ft_glyph = self.ft_face.glyph

            # Store glyph info.
            info = GlyphInfo()
            info.position = [grid.position[0] + grid.padding,
                grid.position[1] + grid.padding]
            info.size = [ft_glyph.bitmap.width, ft_glyph.bitmap.rows]
            info.offset = [ft_glyph.bitmap_left, ft_glyph.bitmap_top - info.size[1]]
            info.advance = [ft_glyph.advance.x / 64.0, ft_glyph.advance.y / 64.0]
            glyphs[chr(c)] = info

            image = numpy.asarray(ft_glyph.bitmap.buffer, numpy.uint8).reshape(
                ft_glyph.bitmap.rows, ft_glyph.bitmap.width)

            if self.ft_face_hires != None and min(image.shape) > 0:
                # Compute and store downsampled signed distance transform of
                # hi-res glyph image.
                self.ft_face_hires.load_char(chr(c))
                ft_glyph_hires = self.ft_face_hires.glyph

                image = numpy.asarray(ft_glyph_hires.bitmap.buffer, numpy.uint8).reshape(
                    ft_glyph_hires.bitmap.rows, ft_glyph_hires.bitmap.width)

                scaling = self.ft_face_hires.size.x_ppem / float(self.ft_face.size.x_ppem)
                padding_hires = int(grid.padding * scaling)
                image_padded = numpy.zeros([image.shape[0] + 2 * padding_hires,
                    image.shape[1] + 2 * padding_hires], numpy.uint8)
                image_padded[padding_hires:image.shape[0] + padding_hires,
                    padding_hires:image.shape[1] + padding_hires] = image

                image_sdt = imageutils.signed_distance_transform(image_padded)

                image_sdt = scipy.ndimage.gaussian_filter(image_sdt, 1.6) #TODO
                image_sdt_downsampled = scipy.misc.imresize(image_sdt,
                    (info.size[1] + 2 * grid.padding, info.size[0] + 2 * grid.padding), 'nearest')

                texture[info.position[1] - grid.padding:info.position[1] + info.size[1] + grid.padding,
                    info.position[0] - grid.padding:info.position[0] + info.size[0] + grid.padding] = image_sdt_downsampled
            else:
                # Store normal resolution glyph image.
                texture[info.position[1]:info.position[1]+info.size[1],
                        info.position[0]:info.position[0]+info.size[0]] = image

            # Update grid position for next glyph.
            grid.advance()

        # Store atlas.
        self.texture = texture
        self.glyphs = glyphs

    def export(self, png_filename, json_filename):
        """Exports font atlas to PNG + JSON format."""
        # Save atlas texture.
        mpl.image.imsave(png_filename, self.texture, vmin=0, vmax=255,
            cmap=mpl.cm.gray)

        # Create JSON for loading the atlas.
        root = {
            'type_name' : 'FontAtlas',
            'font_size' : self.ft_face.size.y_ppem,
            'padding' : self.padding,
            'texture' : {
                'type_name' : 'Texture2D',
                'target' : 'GL_TEXTURE_2D',
                'image' : os.path.join("images/" + png_filename),
                'width' : self.texture.shape[0],
                'height' : self.texture.shape[1],
                'internalformat' : 'GL_RGB8',
                'format' : 'GL_RGB',
            },
            'glyphs' : {},
        }
        for key, value in self.glyphs.items():
            root['glyphs'][key] = value.__dict__

        with open(json_filename, 'w') as file:
            file.write(json.dumps(root, indent=4))


def create_atlas(ft_face, size=[512, 512], padding=0, ft_face_hires=None):
    """Creates a font atlas of size [w, h]."""
    atlas = FontAtlas()
    atlas.ft_face = ft_face
    atlas.ft_face_hires = ft_face_hires
    atlas.padding = padding
    atlas.size = size
    atlas.update()
    return atlas
