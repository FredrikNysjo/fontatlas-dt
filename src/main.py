#!/usr/bin/env python
"""
.. module:: main
   :platform: Unix
   :synopsis: Image/font atlas generator command line tool

.. moduleauthor:: Fredrik Nysjo

"""

import imageutils
import imageatlas
import fontutils
import fontatlas

import sys
import argparse
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-path', required=True,
        help='input path to TrueType font or image directory')
    parser.add_argument('-o', '--output-basename', required=True,
        help='output base name for generated JSON and PNG files')
    parser.add_argument('-s', '--atlas-size', default=512, type=int,
        help='output atlas size in pixels (default=512)')
    parser.add_argument('--font-size', default=38, type=int,
        help='input font size in pixels (default=38)')
    parser.add_argument('--image-scaling', default=1.0, type=float,
        help='scaling applied to input images in image atlas (default=1.0)')
    parser.add_argument('-p', '--padding', default=5, type=int,
        help='output atlas object padding in pixels (default=5)')
    parser.add_argument('-d', '--distance-field', action='store_true',
        help='compute distance fields from hi-res images')

    args = parser.parse_args()
    if os.path.exists(args.input_path) == False:
        sys.exit('Input path does not exist')

    atlas = None
    if os.path.isdir(args.input_path) == True:
        print('Generating image atlas...')
        images = imageutils.load_pngs_in_dir(args.input_path)
        atlas = imageatlas.create_imageatlas(images, args.image_scaling,
            [args.atlas_size, args.atlas_size], args.padding)
        print('Done');
    elif (os.path.splitext(args.input_path)[1] == '.ttf'):
        print('Generating font atlas...')
        face = fontutils.load_face(args.input_path, [args.font_size,
            args.font_size])
        if args.distance_field == True:
            face_hires = fontutils.load_face(args.input_path, [256, 256]) # TODO
            atlas = fontatlas.create_atlas(face, [args.atlas_size,
                args.atlas_size], args.padding, face_hires)
        else:
            atlas = fontatlas.create_atlas(face, [args.atlas_size,
                args.atlas_size], args.padding)
        print('Done');
    else:
        sys.exit('Input is not a TrueType font or an image directory')

    if atlas != None:
        atlas.export(args.output_basename + '.png',
            args.output_basename + '.json')

    sys.exit()


if __name__ == '__main__':
    main()
