"""
.. module:: imageutils
   :platform: Unix
   :synopsis: Image utilities.

.. moduleauthor:: Fredrik Nysjo

"""

import sys
import os

import numpy
import scipy
import scipy.ndimage
import matplotlib as mpl
import matplotlib.image


def load_pngs_in_dir(dirpath):
    """Loads all PNG images in a directory into a dictionary."""
    assert(os.path.exists(dirpath) == True)
    assert(os.path.isdir(dirpath) == True)

    files = [os.path.join(dirpath, f) for f in os.listdir(dirpath)]

    image_files = []
    for f in files:
        if os.path.splitext(f)[1].lower() == '.png':
            image_files.append(f)

    images = {}
    for f in image_files:
        assert(os.path.isfile(f) == True)
        name = os.path.splitext(os.path.basename(f))[0]
        image = mpl.image.imread(f)
        if image != None:
            assert(len(image.shape) == 3)
            image = image[:,:,0]  # Keep only red channel.
            assert(image.dtype == 'float32')
            image = 255.0 * image  # Scale to range 0-255.
            images[name] = image.astype(numpy.uint8)

    return images


def signed_distance_transform(image, threshold=0.5):
    """Computes the signed EDT of an image after thresholding."""
    bg = scipy.ndimage.distance_transform_edt(image < threshold)
    fg = scipy.ndimage.distance_transform_edt(image >= threshold)
    signed_distances = 1.0 * (fg - bg)
    return numpy.clip(signed_distances + 127.0, 0.0, 255.0).astype(numpy.uint8)
