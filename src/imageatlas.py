"""
.. module:: imageatlas
   :platform: Unix
   :synopsis: Image atlas module.

.. moduleauthor:: Fredrik Nysjo

"""

import imageutils

import sys
import os
import json

import numpy
import scipy
import scipy.ndimage
import matplotlib as mpl
import matplotlib.image


class ImageInfo(object):
    """Stores image info.

    Attributes:
        position: Position [x, y] in atlas, in pixels.
        size: Size [w, h] in atlas, in pixels.
    """

    def __init__(self):
        self.position = [0.0, 0.0]
        self.size = [0.0, 0.0]


class Grid(object):
    """Grid function used by ImageAtlas.

    Attributes:
        bounds: Grid bounds, e.g., [512, 512].
        cell_size: Grid cell size, including padding.
        position: Current grid position.
        padding: Object padding in grid
    """

    def __init__(self, bounds, cell_inner_size, padding):
        self.bounds = bounds
        self.cell_size = [cell_inner_size[0] + 2 * padding,
            cell_inner_size[1] + 2 * padding]
        self.position = [0, 0]
        self.padding = padding

    def advance(self):
        """Advance grid position."""
        self.position[0] += self.cell_size[0]
        if ((self.position[0] + self.cell_size[0]) / self.bounds[0]) >= 1:
            self.position[0] = 0
            self.position[1] += self.cell_size[1]
        if ((self.position[1] + self.cell_size[1]) / self.bounds[1]) >= 1:
            sys.exit("Error: Position outside grid bounds")


class ImageAtlas(object):
    """Stores an image atlas.

    Attributes:
        size: Atlas texture size.
        texture: Atlas texture.
        images: Image to create atlas from.
        image_scaling: Amount of scaling to apply to images.
        padding: Image padding in pixels.
        tiles: Items stored in atlas.
    """

    def __init__(self):
        self.size = [512, 512]
        self.texture = None
        self.images = {}
        self.image_scaling = 1.0
        self.padding = 0.0
        self.tiles = {}

    def update(self):
        """Update atlas from current set of input."""
        # Generate grid function for atlas.
        cell_inner_size = [1, 1]
        for image in self.images.values():
            cell_inner_size[0] = max(cell_inner_size[0], image.shape[0])
            cell_inner_size[1] = max(cell_inner_size[1], image.shape[1])
        #cell_inner_size = [self.image_scaling * x for x in cell_inner_size]
        grid = Grid(self.size, cell_inner_size, self.padding)

        # Generate new atlas.
        texture = numpy.zeros(self.size, numpy.uint8)
        tiles = {}
        for name, image in self.images.items():
            # Store image info.
            info = ImageInfo()
            info.position = [grid.position[0] + grid.padding,
                grid.position[1] + grid.padding]
            info.size = [image.shape[0], image.shape[1]]
            tiles[name] = info

            # Store image or signed distance field of image.
            #image_dt = imageutils.signed_distance_transform(image)
            texture[info.position[1]:info.position[1]+info.size[1],
                info.position[0]:info.position[0]+info.size[0]] = image

            # Update grid position for next item.
            grid.advance()

        # Store atlas.
        self.texture = texture
        self.tiles = tiles

    def export(self, png_filename, json_filename):
        """Exports atlas to PNG + JSON format."""
        # Save atlas texture.
        mpl.image.imsave(png_filename, self.texture, vmin=0, vmax=255,
            cmap=mpl.cm.gray)

        # Save atlas metadata.
        root = {
            'type_name' : 'ImageAtlas',
            'padding' : self.padding,
            'texture' : {
                'type_name' : 'Texture2D',
                'target' : 'GL_TEXTURE_2D',
                'image' : os.path.join("images/" + png_filename),
                'width' : self.texture.shape[0],
                'height' : self.texture.shape[1],
                'internalformat' : 'GL_RGB8',
                'format' : 'GL_RGB',
            },
            'images' : {},
        }
        for key, value in self.tiles.items():
            root['images'][key] = value.__dict__
        with open(json_filename, 'w') as file:
            file.write(json.dumps(root, indent=4))


def create_imageatlas(images, image_scaling, size, padding):
    """Creates image atlas of size [w, h]."""
    assert(image_scaling > 0.0)

    atlas = ImageAtlas()
    atlas.images = images
    atlas.image_scaling = image_scaling
    atlas.size = size
    atlas.padding = padding
    atlas.update()
    return atlas
